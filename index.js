import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.5/firebase-app.js";
import { getAuth, signInWithPopup, GoogleAuthProvider, signOut } from "https://www.gstatic.com/firebasejs/9.6.5/firebase-auth.js";
import { getDatabase, ref, set, get, child, remove } from "https://www.gstatic.com/firebasejs/9.6.5/firebase-database.js";
// here it must be the configuration of firebase but i had problems with My pc sir in last minute  ,
// so i couldn't link my code to firebase, I had a problem with it in some previous projects 
//but I really hope that will accept my code 
const firebaseConfig = {
    apiKey: "",
    authDomain: "",
    databaseURL: "",
    projectId: "",
    storageBucket: "",
    messagingSenderId: "",
    appId: "0:0"
};

// Initialization of FireBase
const app = initializeApp(firebaseConfig);
const provider = new GoogleAuthProvider();
const auth = getAuth();
const database = getDatabase(app);
var showFavourites = false

var current_username = null
var currentData = null

var beginening = new Vue({
    el: '#beginening',
    data: {
        hidden: false
    },
    methods: {
        googleLogin: function () {
            console.log("SignIn")
            signInWithPopup(auth, provider)
                .then((result) => {
                    // To acces to google API
                    const credential = GoogleAuthProvider.credentialFromResult(result);
                    const token = credential.accessToken;
                    // User Infos (for the signed user )
                    const user = result.user;
                    current_username = user["ShowName"]
                    navbar.user = current_username
                    navbar.hidden = false
                    beginening.hidden = true
                    Tab.hidden = false
                    console.log("Succed")

                   
                }).catch((error) => {
                   
                    const errorCode = error.code;
                    const credential = GoogleAuthProvider.credentialFromError(error);
                    console.log("Failed Login")
                });

        }
    }
 })

// here we display all the movies 
function writeAllMovies(Id, title, year, genre, path = 'movies-list/') {
    const db = getDatabase();
    set(ref(db, path + Id), {
        genre: genre,
        title: title,
        year: year
       
    });
}


function deleteAllMovies(complete_path) {
    const db = getDatabase();
    console.log("Delete: " + complete_path)
    set(ref(db, complete_path), {
        title: null,
        year: null,
        genre: null
    });
}
var navbar = new Vue({
    el: '.navbar',
    data: {
        user: "",
        hidden: true
    },
    methods: {
        logout: function () {
            const auth = getAuth();
            signOut(auth).then(() => {
                navbar.user = ""
                navbar.hidden = true
                beginening.hidden = false
                Tab.hidden = true
                TabUser.hidden = true
                removeRows("actuelle")
                removeRows("actuelle_user")
                getMovies("pas_chemin", "actuelle")
            }).catch((error) => {
                console.log("Logout failed")
            });

        },
        favourites: function () {
            if (showFavourites) {
                TabUser.hidden = true
                Tab.hidden = false
                removeRows("actuelle_user")

            } else {
                Tab.hidden = true
                getMovies(current_username, "actuelle_user")
                TabUser.hidden = false
                removeRows("actuelle")
                getMovies("pas_chemin", "actuelle")
            }

            showFavourites = !showFavourites
        }
    }
})


var Tab = new Vue({
    el: '#Tab',
    data: {
        hidden: true
    },
    methods: {
        filterTable: function () {
            console.log("lets Filter")
            filterTable("Tab", "myInput")
        }
    }
})


var TabUser = new Vue({
    el: '#TabUser',
    data: {
        hidden: true
    },
    methods: {
        filterTable: function () {
            console.log("lets Filter")
            filterTable()
        }
    }
})

// Now here we will fetch moovies from the json file 
async function fetchJson() {
    const response = await fetch("movies.json")
    const data = await response.json()
    const movie_array = data["movies-list"]
    for (const element of movie_array) {
        writeAllMovies(element["id"], element["title"], element["year"], element["genre"])
    }
}

function getMovies(path, table_name) {
    const dbRef = ref(getDatabase());
    console.log(path, table_name)
    if (path == "pas_chemin") {
        rowUpdate(table_name, currentData)
        return
    }
    get(child(dbRef, path)).then((snapshot) => {
        if (snapshot.exists()) {
            var data = snapshot.val()
            if (table_name == "actuelle") {
                currentData = data
            }
            rowUpdate(table_name, data)
        } else {
            console.log("Nothing to display here !! ");
        }
    }).catch((error) => {
        console.error(error);
    });
}
getMovies(`movies-list`, "actuelle")


function filterTable(table, input) {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById(input);
    filter = input.value.toUpperCase();
    table = document.getElementById(table);
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}
// When the user press on like movie 
function like(Id) {
    document.getElementById(Id + "actuelle").style.display = "none";
    writeAllMovies(Id, currentData[Id]["title"], currentData[Id]["year"], currentData[Id]["genre"], (current_username + '/'))
}
// when the user press on dislike movie 
function dislike(Id) {
    document.getElementById(Id + "actuelle_user").style.display = "none";
    deleteAllMovies((current_username + '/' + Id))
}
// To delete movies 
function removeRows(table_name) {
    const elements = document.getElementsByClassName(table_name + "deleteRow");
    while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
    }
}
// Mettre à jour un film 
function rowUpdate(table_name, data) {
    for (const [key, value] of Object.entries(data)) {

        var table = document.getElementById(table_name);
        var row = table.insertRow(1);
        row.classList.add(table_name + "deleteRow")
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4)

        cell1.innerHTML = key;
        cell2.innerHTML = value["title"];
        cell3.innerHTML = value["year"];
        cell4.innerHTML = value["genre"];
        cell5.innerHTML = `<button type="button" class="btn btn-light like_button" id="${key}${table_name}"> <img src="https://img.icons8.com/material-rounded/24/000000/like--v1.png"/></button>`

        if (table_name == "actuelle") {
            function myFunction() {
                like(key)
            }
            document.getElementById(key + table_name).addEventListener("click", myFunction)
        } else if (table_name == "actuelle_user") {
            function myFunction() {
                dislike(key)
            }
            document.getElementById(key + table_name).addEventListener("click", myFunction)
        }

    }
}
